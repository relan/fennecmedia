What's this?
------------

Metadata and media files used by [Fennec F-Droid](https://f-droid.org/packages/org.mozilla.fennec_fdroid/) at run time.

Licenses
--------

Wallpapers are licensed under the [Unsplash License](https://unsplash.com/license).
